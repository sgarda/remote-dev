#!/bin/bash
 
if [[ $(lsof -i:6006 | wc -l) -eq 0 ]];
then
   source /vol/home-vol3/wbi/gardasam/venvm/bin/activate
   tensorboard --logdir /vol/home-vol3/wbi/gardasam/data/experiments --port 6006 > /vol/home-vol3/wbi/gardasam/.tensorboard.log 2>&1&
   echo "#WELCOME: Start Tensorboard on port 6006"
else
   echo "#WELCOME: Tensorboard is running on port 6006"
fi

