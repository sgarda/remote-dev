#!/bin/bash
# sync to remote instance provided to the current project (as defined by git repo) .
# The remote instance should be defined by the alias associated in the ~/.ssh/config file.

show_help()
{
   echo ""
   echo "Pull the repository version from a remote instance."
   echo "Only files in the folder named 'results' will be retrieved"
   echo ""
   echo "Usage: $0 -u <remote_user> -r <remote_alias>"
   echo -e "\t-u Username in remote instance"
   echo -e "\t-r Remote instance alias name (defined in '~/.ssh/config')"
   echo -e "\n"
   echo "Prerequisite:"
   echo -e "\t 1) Access to <remote_alias> configured in '~/.ssh/config'"
   echo -e "\t 2) Identity file path is '~/.ssh/ids/<remote_alias>.pem'"
   echo -e "\t 3) Folder with project in remote instance is in '~/projects'"
   exit 1 # Exit script after printing help
}


while getopts h?u:r: option
do
case "${option}"
in
h|\?)
        show_help
        exit 0
        ;;
u) REMOTE_USER=${OPTARG};;
r) REMOTE_ALIAS=${OPTARG};;
esac
done

if [ ! "$REMOTE_USER" ] || [ ! "$REMOTE_ALIAS" ]
then
    show_help
    exit 1
fi


# get the path to the project root (where the .git folder is)
PROJECT_PATH=`git rev-parse --show-toplevel 2> /dev/null`
PROJECT_NAME=`basename $PROJECT_PATH`
REMOTE_INSTANCE_URL=$(ssh -G $REMOTE_ALIAS | awk '$1 == "hostname" { print $2 }')
SHH_ID_FILE=~/.ssh/ids/$REMOTE_ALIAS.pem


if [ $? -eq 0 ]; then
# rsync from server ~/projects/project-name including only 'notebooks' and results 'folder'
rsync -avL --cvs-exclude --progress --include 'results/' --include 'results/***' --exclude '*' -e "ssh -i $SHH_ID_FILE" $REMOTE_USER@$REMOTE_INSTANCE_URL:~/projects/$PROJECT_NAME/ $PROJECT_PATH 
fi
