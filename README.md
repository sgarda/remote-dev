# Remote Developing setup

Script for remote developing. Improvement on script found in this great [post](https://matttrent.com/remote-development/).

## Push

Push via rsync *all* files of your local git repo (besides folder "notebooks") to a remote server.

---

Usage: ./remote-push.sh -r <remote_alias>

	-r Remote instance alias name (defined in '~/.ssh/config')


Prerequisite:

	 1) Access to <remote_alias> configured in '~/.ssh/config'

	 2) Identity file path is '~/.ssh/ids/<remote_alias>.pem'

---

## Pull

Pull via rsync folder "notebooks" and its content into the local copy of your git repo.

---

Usage: ./remote-pull.sh -r <remote_alias>

	-u Username in remote instance

	-r Remote instance alias name (defined in '~/.ssh/config')



Prerequisite:

	 1) Access to <remote_alias> configured in '~/.ssh/config'

	 2) Identity file path is '~/.ssh/ids/<remote_alias>.pem'

	 3) Folder with project in remoote instance is '~/projects'

---

